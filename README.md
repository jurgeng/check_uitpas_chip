# UiTPAS chip check

De bedoeling van deze scripts, is om op een gemakkelijke manier:
* **valideren** of chipnummers en UiTPAS-nummers van een groot lot kaarten klopt
* **genereren** van een betrouwbare lijst van chipnummers en UiTPAS-nummers om in te laden in een kaartsysteem

## Gebruikte hardware
### NFC-lezer
Voor het inlezen van de NFC-codes, wordt een [ACR122U NFC-lezer](https://www.acs.com.hk/en/products/3/acr122u-usb-nfc-reader/) gebruikt.

De NFC-library in Python ondersteunt mogelijk ook andere kaartlezer, maar deze werden op dit moment niet getest.

### barcode scanner
Voor dit project werd gebruik gemaakt van de Sparkfun 2D [Bardcode Scanner Breakout board](https://learn.sparkfun.com/tutorials/2d-barcode-scanner-breakout-hookup-guide/introduction).

Merk op dat de scanner moet ingesteld worden op [USB-COM mode](https://learn.sparkfun.com/tutorials/2d-barcode-scanner-breakout-hookup-guide/de2120-python-package).

## Installatie
### Algemene instellingen
Dit programma is geschreven in Python 3 (getest op Python 3.10) en maakt gebruik van een aantal libraries beschikbaar op PyPi:
- **[nfcpy](https://pypi.org/project/nfcpy/)** voor het aansturen van de NFC-lezer
- **[de2120-barcode-scanner](https://pypi.org/project/de2120-barcode-scanner/)** voor het aansturen van de barcode scanner
- [pyserial](https://pypi.org/project/pyserial/) voor het expliciet kunnen instellen van de juiste seriële poort
- [argparse (core)](https://docs.python.org/3/library/argparse.html) om parameters te kunnen ingeven aan de command line
- [csv (core)](https://docs.python.org/3/library/csv.html) om het CSV-bestand in te lezen en op te slaan
- [sys (core)](https://docs.python.org/3/library/argparse.html) voor interacties met het bestandssysteem
- [collections (core)](https://docs.python.org/3/library/collections.html) om de CSV-lijst te sorteren

Globaal dus:
```
pip install nfcpy de2120-barcode-scanner pyserial
```

### Linux
De gebruiker die moet communiceren met de scanner, moet lid zijn van de dialout groep

### Windows


## Gebruik
