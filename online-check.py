# This script only works for UiTPAS cards that are in use, so not for LOCAL_STOCK
# (rendering it useless in this tool at this moment)
# If functionality changes, feature will be implemented in regular cardcheck.py

import nfc
import http.client
import json
import credentials
# This file should contain:
# auth_server = "https://..."
# client_id = "ENTER_CLIENT_HERE"
# client_secret = "ENTER CLIENT_SECRET_HERE"


# uitpasAuthenticate takes care of the oAuth 2.0 Client Authentication through UiTiDv2
# credentials are to be found in accompanying file credentials.py
def uitpas_authenticate(id, secret):
    conn = http.client.HTTPSConnection(credentials.auth_server)
    payload = "{\n  \"client_id\": \"" + id + "\",\n  \"client_secret\": \"" + secret + "\",\n  \"audience\": " \
              "\"https://api.publiq.be\",\n  \"grant_type\": \"client_credentials\"\n}"
    headers = {'Content-Type': "application/json"}
    conn.request("POST", "/oauth/token", payload, headers)
    res = conn.getresponse()
    data = json.loads(res.read().decode("utf-8"))
    try:
        if res.code == 200:
            print("+ CONNECTED")
            return "Bearer " + data["access_token"]
        else:
            print("! FAILED TO CONNECT")
            data = res.read()
            print(data.decode("utf-8"))
            return False
    except (OSError, IOError) as e:
        print("! CONNECTION FAILED - just scanning, validate manually")
        print(e.message)
        return False


def chip_belongs_to_uitpas(chip):
    bearer = uitpas_authenticate(credentials.client_id, credentials.client_secret)
    conn = http.client.HTTPSConnection("api.uitpas.be")

    headers = {
        'Content-Type': "application/json",
        'Authorization': bearer
    }
    try:
        conn.request("GET", "/chip-numbers/"+chip, headers=headers)
        res = conn.getresponse()
        data = json.loads(res.read().decode("utf-8"))
        if data['status'] == 200:
            print("UiTPAS volgens chipnummer: ", data["uitpasNumber"])
        elif data['status'] == 404:
            print("! ERROR: ", data['detail'])
        else:
            print('! ERROR ', data['status'], ": ", data['title'], " - ", data['detail'])
    except Exception as e:
        print("! ERROR: ", e)


def on_connect(tag):
    print("+ CHIP FOUND", tag.identifier.hex())

    # Embed scanning logic here
    results = "BARCODE_VALUE"

    if len(results) > 0:
        result = results[0]
        print('')
        print('+ BARCODE SCANNED:', result.data.decode('utf-8'))
        chip_belongs_to_uitpas(tag.identifier.hex())
    print('.', end='')


with nfc.ContactlessFrontend('usb') as clf:
    print("Waiting for an NFC tag...")
    clf.connect(rdwr={'on-connect': on_connect})
